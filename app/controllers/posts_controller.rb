class PostsController < ApplicationController

  def index
    @posts = Post.order('point desc')
  end

  def update
    p = Post.find(params[:id])
    if params[:plus] == "true"
      p.point = p.point + 1
    elsif params[:minus] == "true"
      p.point = p.point - 1
    end
    p.save
    redirect_to posts_path
  end

  def destroy
    p = Post.find(params[:id])
    p.destroy
    redirect_to posts_path
  end

  def new
    @post = Post.new
  end

  def create
    Post.create(title: params[:post][:title], description: params[:post][:description], point: 0)
    redirect_to posts_path
  end
end

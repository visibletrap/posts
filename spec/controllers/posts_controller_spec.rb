require 'spec_helper'

describe PostsController do
  describe "index" do
    it "returns real posts" do
      p = Post.create
      get :index
      expect(assigns(:posts).count).to eq(1)
      expect(assigns(:posts).first).to eq(p)
    end

    it "returns posts sort by point" do
      p = Post.create(point: 1)
      q = Post.create(point: 2)
      get :index
      expect(assigns(:posts).first).to eq(q)
      expect(assigns(:posts).last).to eq(p)
    end
  end

  describe "update" do
    context "plus = true" do
      it "increments post point by 1" do
        p = Post.create(point: 1)
        put :update, id: p.id, plus: 'true'
        expect(p.reload.point).to eq(2)
      end
    end

    context "minus = true" do
      it "minus post point by 1" do
        p = Post.create(point: 2)
        put :update, id: p.id, minus: 'true'
        expect(p.reload.point).to eq(1)
      end
    end
  end

  describe "destroy" do
    it "delete post" do
      p = Post.create
      delete :destroy, id: p.id
      expect(Post.count).to eq(0)
    end
  end

  describe "create" do
    it "create post" do
      post :create, post: { title: "abc", description: "AAAAAAAA" }
      expect(Post.count).to eq(1)
      expect(Post.first.title).to eq("abc")
      expect(Post.first.description).to eq("AAAAAAAA")
      expect(Post.first.point).to eq(0)
    end
  end
end
